
```
#!bash

#export SK_TARGET_DEVICE=t677
#export SK_TARGET_DEVICE=t670

export SK_ROOTDIR=$HOME/dev/sm-gv
export SK_IMGTOOLS=$SK_ROOTDIR/mkbootimg_tools
export SK_GCC=$SK_ROOTDIR/gcc-tools-4.9
export SK_DIR=$SK_ROOTDIR/kernel-lp
export SK_OUT=$SK_DIR/out

if [ "$SK_TARGET_DEVICE" == "t677" ]; then
  export SK_BUILD_SCRIPT=$SK_DIR/mk_677.sh
  export SK_DTS_FILENAME="exynos7580-gvlte_eur_open_rev00"
  export SK_KERNEL_SUFFIX=T677XXU1AOJC
elif [ "$SK_TARGET_DEVICE" == "t670" ]; then
  export SK_BUILD_SCRIPT=$SK_DIR/mk_670.sh
  export SK_DTS_FILENAME="exynos7580-gvwifi_rev10"
  export SK_KERNEL_SUFFIX=T670UEU2APJ1
else
  echo "ERROR: var SK_TARGET_DEVICE have unknown content"
  exit 4
fi

export SK_BOOTDIRORIG=$SK_ROOTDIR/boot-gv
export SK_BOOTORIG=$SK_BOOTDIRORIG/boot-$SK_KERNEL_SUFFIX.img
export SK_BOOTDIR=$SK_IMGTOOLS/boot_

#-----------------------------------

mkdir -p $SK_ROOTDIR
cd $SK_ROOTDIR

if [ ! -d "$(basename $SK_IMGTOOLS)" ]; then
  git clone --branch "master" --progress -v "https://github.com/xiaolu/mkbootimg_tools.git" $(basename $SK_IMGTOOLS)
fi
if [ ! -d "$(basename $SK_BOOTDIRORIG)" ]; then
  git clone --progress -v "https://bitbucket.org/smkernel/boot-gv.git" $(basename $SK_BOOTDIRORIG)
fi
if [ ! -d "$(basename $SK_GCC)" ]; then
  git clone --progress -v "https://remittor@bitbucket.org/UBERTC/aarch64-linux-android-4.9-kernel" $(basename $SK_GCC)
fi
if [ ! -d "$(basename $SK_DIR)" ]; then
  git clone --progress -v "https://bitbucket.org/smkernel/android_kernel_samsung_gvlte.git" $(basename $SK_DIR)
fi

#-----------------------------------

cd $SK_IMGTOOLS
rm -rf $SK_BOOTDIR
cp -f $SK_BOOTORIG $SK_IMGTOOLS/boot_orig.img
./mkboot boot_orig.img $(basename $SK_BOOTDIR)

cd $SK_BOOTDIR/ramdisk
sed -i "s/ro.secure=1/ro.secure=0/" ./default.prop
sed -i "s/ro.debuggable=0/ro.debuggable=1/" ./default.prop
sed -i "s/ro.adb.secure=1/ro.adb.secure=0/" ./default.prop
sed -i "s/persist.sys.usb.config=mtp/persist.sys.usb.config=adb,mtp/" ./default.prop
echo "ro.securestorage.support=false" >> ./default.prop
sed -i "s/wait,verify/wait/" ./fstab.samsungexynos7580
#sed -i "s/wait,check,encryptable=footer/wait,check/" ./fstab.samsungexynos7580

cd $SK_DIR
chmod 775 ./build_kernel.sh
chmod 775 $SK_BUILD_SCRIPT
chmod 775 ./scripts/dtbTool/dtbTool

#-----------------------------------

cd $SK_DIR
rm -rf $SK_OUT

cd $SK_DIR
export OUT_DIR=$(basename $SK_OUT)

export CROSS_COMPILE=$SK_GCC/bin/aarch64-linux-android-
$SK_BUILD_SCRIPT

#-----------------------------------

cd $SK_DIR
ARCH=arm64
DTSDIR=$SK_DIR/arch/$ARCH/boot/dts
DTSOUT=$SK_DIR/$OUT_DIR/arch/$ARCH/boot
DTBDIR=$DTSOUT/dts
INCDIR=$SK_DIR/include
DTCTOOL=$SK_DIR/$OUT_DIR/scripts/dtc/dtc
DTSFILES=$SK_DTS_FILENAME
PAGE_SIZE=2048
DTB_PADDING=0
mkdir -p $DTBDIR
cd $DTBDIR
rm -f ./*
for dts in $DTSFILES; do
	echo "=> Processing: ${dts}.dts"
	${CROSS_COMPILE}cpp -nostdinc -undef -x assembler-with-cpp -I "$INCDIR" "$DTSDIR/${dts}.dts" > "${dts}.dts"
	echo "=> Generating: ${dts}.dtb"
	$DTCTOOL -p $DTB_PADDING -i "$DTSDIR" -O dtb -o "${dts}.dtb" "${dts}.dts"
done 
echo "Generating dtb.img..."
$SK_DIR/scripts/dtbTool/dtbTool -o "$DTSOUT/dtb.img" -d "$DTBDIR/" -s $PAGE_SIZE
  
#-----------------------------------

cp -f $SK_OUT/arch/arm64/boot/dtb.img $SK_BOOTDIR/dt.img

cp -f $SK_OUT/arch/arm64/boot/Image $SK_BOOTDIR/kernel

cd $SK_IMGTOOLS
./mkboot $(basename $SK_BOOTDIR) boot.img
echo -n "SEANDROIDENFORCE" >> boot.img
#cp -f boot.img recovery.img
cd $SK_DIR

```